// Variables
var pressureLabel, temperatureLabel;
var weatherConnectionURL = 'http://private-4945e-weather34.apiary-proxy.com/weather34/rain';
var days;
var pressure;
var temperature;

// Event Handlers
function pressureChanged(pressureValue) {
  pressureLabel.innerHTML = pressureValue;
  pressure = pressureValue;
  drawChanceOfRainChart(); // Only this chart is rerendered because Amount of Rainfall is independent from value.
}

function temperatureChanged(temperatureValue) {
  temperatureLabel.innerHTML = temperatureValue;
  temperature = temperatureValue;
  drawChanceOfRainChart(); // Only this chart is rerendered because Amount of Rainfall is independent from value.
}

// Calculations
function chanceOfRain(pressure, temperature, amount) {
  var score = Math.log(amount + 1) * Math.log(pressure - 929) * Math.log(temperature - 9);
  var mean = Math.min(Math.max(score, 0), 100);
  var upper_bound = Math.min(1.5 * mean, 100);
  var lower_bound = Math.max(0.5 * mean, 0);
  return [lower_bound, mean, upper_bound];
}

// Init Data
function getAmountOfRainfallByDay() {
  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function () {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      days = JSON.parse(xhttp.responseText)[0].days;
      drawCharts();
    }
  };

  xhttp.open('GET', weatherConnectionURL, true);
  xhttp.send();
}

// Draw Amount of rainfall chart
// Documentation: https://developers.google.com/chart/interactive/docs/gallery/barchart
function drawAmountOfRainFallChart() {
  var dataset = [
    ['Day', 'Amount']
  ];
  days.forEach(function (item, index) {
    dataset.push([item.day, item.amount]);
  });
  var data = google.visualization.arrayToDataTable(dataset);
  var options = {
    legend: { position: 'none' },
    vAxis: {
      'minValue': 0,
      'maxValue': 100
    }
  };
  var chart = new google.charts.Bar(document.getElementById('columnchart-material-amount-of-rainfall'));

  chart.draw(data, options);
}

// Draw Chance of rain chart
// Documentation: https://developers.google.com/chart/interactive/docs/gallery/linechart
function drawChanceOfRainChart() {
  var dataset = [
    ['Day', 'Lower Bound', 'Mean', 'Upper Bound']
  ];
  days.forEach(function (item, index) {
    var chanceOfRainResults = chanceOfRain(pressure, temperature, item.amount);
    dataset.push([item.day, chanceOfRainResults[0], chanceOfRainResults[1], chanceOfRainResults[2]]);
  });

  var data = google.visualization.arrayToDataTable(dataset);

  var options = {
    curveType: 'function',
    legend: { position: 'bottom' },
    vAxis: {
      'minValue': 0,
      'maxValue': 100
    }
  };

  var chart = new google.visualization.LineChart(document.getElementById('curve-chart-chance-of-rainfall'));

  chart.draw(data, options);
}

// Rerender all charts helper function
function drawCharts() {
  drawAmountOfRainFallChart();
  drawChanceOfRainChart();
}

function init() {
  // Set init values
  pressure = 1000;
  temperature = 20;

  // Store labels into Variables (no need for repeated DOM-retrieve)
  pressureLabel = document.getElementById('pressure-label');
  temperatureLabel = document.getElementById('temperature-label');

  // Init Charts
  google.charts.load('current', { 'packages': ['bar', 'corechart'] });
  google.charts.setOnLoadCallback(getAmountOfRainfallByDay);
}

